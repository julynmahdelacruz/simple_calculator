import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

const Color themeColor1 = Color(0xffD2CAD8);
const Color themeColor2 = Color(0xffEAB274);
const Color themeColor3 = Color(0xffA8736B);
const Color themeColor4 = Color(0xff634F40);
const Color splashColor = Colors.white;

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;
  
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  RegExp stringFormatter = RegExp(r"([.]*0+)(?!.*\d)");
  String displayValue = "0";
  double operand1 = 0;
  double operand2 = 0;
  String operatorSign = "";
  
  void _cellEvent(var value) {
    setState(() {
      if(displayValue == "0"){
        displayValue=value;
      }else{
        displayValue=displayValue+value;
      }
    });
  }
  
  void _clearEvent(){
    setState(() {
      if(operand1.toString().isNotEmpty){
        operand1 = 0;
      }
      displayValue = "0";
    });
  }
  
  void _operandEvent(var operator){
    operand1 = double.parse(displayValue);
    displayValue = "0";
    operatorSign = operator; 
  }
  
  void _operationEvent(){
    double result = 0;
    operand2 = double.parse(displayValue);  
    if(operatorSign == "+"){
      result = operand1 + operand2;
    }else if(operatorSign == "-"){
      result = operand1 - operand2;
    }
    else if(operatorSign == "x"){
      result = operand1 * operand2;
    }
    else if(operatorSign == "/"){
      result = operand1 / operand2;
    }
    
    setState(() {
      displayValue = result.toString().replaceAll(stringFormatter, "");
      operand1 = 0;
      operand2 = 0;
    });
  }
  
  
  String operand1Display(){
    return operand1.toString() == "0.0"
    ? ""
    : operand1.toString().replaceAll(stringFormatter, "") + operatorSign;
  }

  @override
  Widget build(BuildContext context) {
    var paddingHoriz = MediaQuery.of(context).size.width * 0.04;
    return Scaffold(
      appBar: AppBar(
        title: Center(
        child: Text("Simple Calculator",
                    style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.height * 0.02),
                    ),
        ),
        backgroundColor: themeColor4.withOpacity(0.3),
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: paddingHoriz,
          right: paddingHoriz,
          top: MediaQuery.of(context).size.height * 0.05,
          bottom: MediaQuery.of(context).size.width * 0.1,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            displayContainer(context),
            SizedBox(height: MediaQuery.of(context).size.height * 0.03,),
            Expanded(
              child: GridView.count(
                physics: NeverScrollableScrollPhysics(),
                crossAxisCount: 4,
                mainAxisSpacing: MediaQuery.of(context).size.height * 0.01,
                crossAxisSpacing: MediaQuery.of(context).size.width * 0.02,
                padding: EdgeInsets.only(
                  left: paddingHoriz,
                  right: paddingHoriz,
                  bottom: MediaQuery.of(context).size.width * 0.1,
                ),
                children: [
                  numbersCell(context, "7", (){
                    _cellEvent("7");
                  }),
                  numbersCell(context, "8", (){
                    _cellEvent("8");
                  }),
                  numbersCell(context, "9", (){
                    _cellEvent("9");
                  }),
                  operandCell(context, "+", (){
                    _operandEvent("+");
                  }),
                  
                  numbersCell(context, "4", (){
                    _cellEvent("4");
                  }),
                  numbersCell(context, "5", (){
                    _cellEvent("5");
                  }),
                  numbersCell(context, "6", (){
                    _cellEvent("6");
                  }),
                  operandCell(context, "-", (){
                    _operandEvent("-");
                  }),
                  
                  numbersCell(context, "1", (){
                    _cellEvent("1");
                  }),
                  numbersCell(context, "2", (){
                    _cellEvent("2");
                  }),
                  numbersCell(context, "3", (){
                    _cellEvent("3");
                  }),
                  operandCell(context, "x", (){
                    _operandEvent("x");
                  }),
                  
                  numbersCell(context, "0", (){
                    _cellEvent("0");
                  }),
                  operandCell(context, ".", (){
                    if(!displayValue.contains(".")){
                      _cellEvent(".");
                    }
                  }),
                  operandCell(context, "=", (){
                    _operationEvent();
                  }),
                  operandCell(context, "/", (){
                    _operandEvent("/");
                  }),
                  
                ],
                ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.01,),
            clearCell(context),
            
          ],
        ),
      ),
      
      backgroundColor: themeColor2,
    );
  }
  
 Widget displayContainer(BuildContext context){
   var paddingHoriz = MediaQuery.of(context).size.width * 0.04;
   Color displayTextColor = Colors.white;
   return Container(
              alignment: Alignment.bottomRight,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.15,
              padding: EdgeInsets.only(
                left: paddingHoriz,
                right: paddingHoriz,
                top: MediaQuery.of(context).size.height * 0.05,
                bottom: MediaQuery.of(context).size.width * 0.03,
              ),
              decoration: BoxDecoration(
                color: themeColor4.withOpacity(0.85),
                borderRadius: BorderRadius.circular(3),
              ),
              child: Column(
                children: [
                   Text(operand1Display(),
                    style: TextStyle(color: displayTextColor, fontSize: MediaQuery.of(context).size.height * 0.02),
                         overflow: TextOverflow.ellipsis,
                          ),
                   Text(displayValue,
                    style: TextStyle(color: displayTextColor, fontSize: MediaQuery.of(context).size.height * 0.05),
                         overflow: TextOverflow.ellipsis,
                          ),
                ],
              )
            );
 }
  
 Widget numbersCell(BuildContext context, String label, Function onTapEvent){
   return InkWell(
     onTap: (){
       onTapEvent();
     },
     borderRadius: BorderRadius.circular(25),
     splashColor: splashColor,
     child: Container(
     alignment: Alignment.center,
     padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
     decoration: BoxDecoration(
       color: themeColor1.withOpacity(0.85),
       borderRadius: BorderRadius.circular(20)
     ),
     child: Text(label,
                 style: TextStyle(color: themeColor4, fontSize: MediaQuery.of(context).size.height * 0.04),
     ),
   ),
   );
 }
 
 Widget operandCell(BuildContext context, String label, Function onTapEvent){
   return InkWell(
     onTap: (){
       onTapEvent();
     },
     borderRadius: BorderRadius.circular(25),
     splashColor: splashColor,
     child: Container(
     alignment: Alignment.center,
     padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
     decoration: BoxDecoration(
       color: themeColor3.withOpacity(0.85),
       borderRadius: BorderRadius.circular(20)
     ),
     child: Text(label,
                 style: TextStyle(color: themeColor4, fontSize: MediaQuery.of(context).size.height * 0.04),
     ),
   ),
   ); 
 }
 
 Widget clearCell(BuildContext context){
   return InkWell(
     onTap: (){
      _clearEvent();
     },
     splashColor: Colors.white,
     borderRadius: BorderRadius.circular(25),
     child: Container(
     alignment: Alignment.center,
     padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
     width: MediaQuery.of(context).size.width * 0.9,
     height: MediaQuery.of(context).size.height * 0.1,
     decoration: BoxDecoration(
       color: themeColor1.withOpacity(0.85),
       borderRadius: BorderRadius.circular(25)
     ),
     child: Text("CLEAR",
                 style: TextStyle(color: themeColor4, fontSize: MediaQuery.of(context).size.height * 0.04),
     ),
   ),
   ); 
 }
}
